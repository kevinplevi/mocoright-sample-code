package com.hash.mocorightdemoapp

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.*
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.view.isInvisible
import com.mocodrm.clientsdk.Mocoright
import com.mocodrm.clientsdk.Mocoright.inputList
import com.github.barteksc.pdfviewer.PDFView
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.result.Result
import com.hash.mocorightdemoapp.databinding.FragmentFirstBinding
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.util.*
import com.google.gson.reflect.TypeToken

import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.lang.reflect.Type
import java.net.HttpURLConnection
import java.net.URL
import kotlin.collections.ArrayList




data class LibraryData(
    val id: String,
    val created_at: String,
    val updated_at: String,
    val label: String,
    val metadata: String
)

data class MocoData(
    val activation_quota: Long,
    val moco_id: String,
    val moco_label: String,
    val moco_metadata: String
)

data class MocoRightData(
    val status: String,
    val download_url: String,
    val moco_right: String
)

data class FileMoco(
    val status: String,
    val fileMDRM: ByteArray?,
)

data class resData(
    val count: Int,
    val data: List<LibraryData>
)

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var attStatus = ""
    private var acID = ""

    //    private var paths = arrayOf("Item1","Item2","Item3")
    private var libraryName = ArrayList<String>()
    private var libraryID = ArrayList<String>()
    private var mocoName = ArrayList<String>()
    private var mocoID = ArrayList<String>()
    val userid = "N456OR"
    private var selectedLibs = ""
    private var selectedMoco = ""
//    var sharedPref : SharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE);

    fun fuelGetLibs() {
        val serverurl = context?.let { getConfigValue(it, "serverurl") }!!
        val (request, response, result) = Fuel.post("${serverurl}/api/entity/get-library").
        body("""{"limit": 1}""").responseString()
        when (result) {
            is Result.Failure -> {
                val ex = result.getException()
                println(ex)
                println(String(response.data))
                println(request)
            }
            is Result.Success -> {
                val data = result.get()
                println("result getapp : $data")

                val dataExample = """{"count":68,"data":[{"id":"b9b5345f-18db-4109-936c-6f9eb14bf66e","created_at":"2022-01-14T04:00:26.146992Z","updated_at":"2022-03-30T06:38:33.450415Z","deleted_at":null,"label":"Perpustakaan Pusat Kota Malang","metadata":""},{"id":"7de26610-a957-4669-8017-005b4ad95b63","created_at":"2022-01-14T04:00:26.146992Z","updated_at":"2022-03-30T06:38:33.450415Z","deleted_at":null,"label":"Perpustakaan Pusat Kota Surabaya","metadata":""}]}"""
                val gson = Gson()
                val type: Type = object : TypeToken<resData>() {}.type
                val appList: resData = gson.fromJson(dataExample, type)
                for (data in appList.data) {
                    libraryName.add(data.label)
                    libraryID.add(data.id)
                }
                var spinner = view?.findViewById(R.id.spinner) as Spinner
                val adapter = context?.let {
                    ArrayAdapter(
                        it,
                        android.R.layout.simple_spinner_item, libraryName
                    )
                }!!
                Handler(Looper.getMainLooper()).post {
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinner.adapter = adapter
                }
            }
        }
    }

    fun fuelGetMoco() {
        val serverurl = context?.let { getConfigValue(it, "serverurl") }!!
        println("Libs - " + selectedLibs)
        val bodyData =
            """{"library_id": "$selectedLibs"}"""

        val (request, response, result) = Fuel.post("${serverurl}/api/entity/get-activation-by-library")
            .header(Headers.CONTENT_TYPE, "application/json")
            .body(bodyData)
            .responseString()
        when (result) {
            is Result.Failure -> {
                val ex = result.getException()
                println(ex)
                println(String(response.data))
                println(request)
            }
            is Result.Success -> {
                val data = result.get()
                println("result getmoco : $data")
                val gson = Gson()
                val type: Type = object : TypeToken<List<MocoData?>?>() {}.type
                val appList: List<MocoData> = gson.fromJson(data, type)
                mocoName.clear()
                mocoID.clear()
                for (data in appList) {
                    mocoName.add(data.moco_label)
                    mocoID.add(data.moco_id)
                }
                var mocoSpin = view?.findViewById(R.id.spinner2) as Spinner
                val adapter = context?.let {
                    ArrayAdapter(
                        it,
                        android.R.layout.simple_spinner_item, mocoName
                    )
                }!!
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                Handler(Looper.getMainLooper()).post {
                    mocoSpin.adapter = adapter
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        GlobalScope.launch(Dispatchers.IO) {
            // ...
            runBlocking {
                val jobB = async { fuelGetLibs() }
                val result = jobB.await()
            }
        }
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        verifyStoragePermissions(activity)
        val appid = context?.let { getConfigValue(it, "appid") }!!
        val appclientpub = context?.let { getConfigValue(it, "appclientpub") }!!
        val serverurl = context?.let { getConfigValue(it, "serverurl") }!!
        val safetynetkey = context?.let { getConfigValue(it, "safetynetkey") }!!
        var sharedPref : SharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE);
        var ignoresafety = true
        //view
        var text1 = view.findViewById<TextView>(R.id.textview_first)
        val pdfView: PDFView = view.findViewById(R.id.pdfView)
        val but1 = view.findViewById<Button>(R.id.button_first)
        val but2 = view.findViewById<Button>(R.id.button_sec)
        //dropdown
        var mocoSpin = view.findViewById(R.id.spinner2) as Spinner
        var spinner = view.findViewById(R.id.spinner) as Spinner
        mocoSpin.isEnabled = false
        spinner.isEnabled = false
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                println("${view?.id} - ${libraryName[position]}")
                Toast.makeText(context, "${libraryName[position]} selected", Toast.LENGTH_SHORT)
                    .show()
                selectedLibs = libraryID[position]
//                Timer().schedule(object : TimerTask() {
//                    override fun run() {
//                        fetchMoco()
//                    }
//                }, 1000 )
                GlobalScope.launch(Dispatchers.IO) {
                    // ...
                    runBlocking {
                        val jobB = async { fuelGetMoco() }
                        val result = jobB.await()
                    }
                }
            }

        }

        mocoSpin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedMoco = mocoID[position]
                var checkMocorightLocal = sharedPref.getString("$acID:$selectedLibs:$selectedMoco:jwt", null)
                println("Hasil check mocoright : "+checkMocorightLocal)
                if(checkMocorightLocal == null) {
                    but1.text = "Borrow"
                } else {
                    but1.text = "Open"
                }
                Toast.makeText(context, "${mocoName[position]} selected", Toast.LENGTH_SHORT).show()
            }
        }
        but1.isEnabled = false
        but2.isEnabled = false
        val mocodrmapipath = "/api"
        //init
        var MocorightClass = activity?.let {
            Mocoright(
                requireContext().applicationContext as Application,
                requireContext(),
                it,
                appclientpub,
                serverurl,
                mocodrmapipath,
                safetynetkey,
                ignoresafety
            )
        }!!

        binding.buttonLogin.setOnClickListener {
            text1.text = "Loading login ......"
            Timer().schedule(object : TimerTask() {
                @SuppressLint("SetTextI18n")
                @RequiresApi(Build.VERSION_CODES.O)
                override fun run() {
                    var login = MocorightClass.login(
                        inputList(
                            appID = appid,
                            userID = userid,
                            pustakaID = selectedLibs
                        )
                    )
                    println("AC-ID : " + login.ac_id)
                    if (login.status === "Success Attestation") {
                        attStatus = login.status
                        acID = login.ac_id
                        Handler(Looper.getMainLooper()).post {
                            text1.text =
                                "Login Status : " + login.status + "\n AttestCertificate ID : " + login.ac_id
                            but1.isEnabled = true
                            but2.isEnabled = false
                            mocoSpin.isEnabled = true
                            spinner.isEnabled = true
                            var checkMocorightLocal = sharedPref.getString("$acID:$selectedLibs:$selectedMoco:jwt", null)
                            println("$acID:$selectedLibs:$selectedMoco:jwt")
                            println("Hasil check mocoright : "+checkMocorightLocal)
                            if(checkMocorightLocal == null) {
                                but1.text = "Borrow"
                            } else {
                                but1.text = "Open"
                            }
                        }
                    } else {
                        Handler(Looper.getMainLooper()).post {
                            text1.text = "Login Error : " + login.status
                            but1.isEnabled = false
                            but2.isEnabled = false

                        }
                    }


                }
            }, 2000)
        }

        binding.buttonReset.setOnClickListener {
            text1.text = "Loading reset ......"
            var sharedPref : SharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE);
            Timer().schedule(object : TimerTask() {
                @RequiresApi(Build.VERSION_CODES.O)
                override fun run() {
                    MocorightClass.resetStorage()
                    sharedPref.edit().clear().apply()
                    attStatus = ""
                    Handler(Looper.getMainLooper()).post {
                        text1.text = "Reset storage success"
                        but2.isEnabled = false
                        but2.text = "HIDE PDF"
                        but1.isEnabled = false
                        text1.visibility = View.VISIBLE
                        spinner.visibility = View.VISIBLE
                        mocoSpin.visibility = View.VISIBLE
                        pdfView.visibility = View.INVISIBLE
                        mocoSpin.isEnabled = false
                        spinner.isEnabled = false
                    }
                }
            }, 1000)
        }


        binding.buttonFirst.setOnClickListener {
            text1.text = "Loading Borrow Moco.............."
            Timer().schedule(object : TimerTask() {
                @SuppressLint("SetTextI18n")
                @RequiresApi(Build.VERSION_CODES.O)
                override fun run() {
                    if (attStatus === "Success Attestation") {
                        //GET MOCORIGHT JWT
                        var mocorightJWT: MocoRightData
                        var checkMocorightLocal = sharedPref.getString("$acID:$selectedLibs:$selectedMoco:jwt", null)
                        println( "Hasil mocoright local" + checkMocorightLocal)
                        if(checkMocorightLocal == null){
                            mocorightJWT = getMocoright()
                        } else {
                            println("MOCORIGHT EXIST")
                            val mainObject = JSONObject(checkMocorightLocal)
                            val downloadUrl = mainObject.getString("download_url")
                            val jwt = mainObject.getString("moco_right")
                            mocorightJWT = MocoRightData(
                                status = "OK",
                                download_url = downloadUrl,
                                moco_right = jwt
                            )
                        }
                        if (mocorightJWT.status == "OK") {
                            var checkMocorightFile = sharedPref.getString("$acID:$selectedLibs:$selectedMoco:file", null)
                            println( "Hasil mocoright file" + checkMocorightFile)
                            var getFile: FileMoco

                            if(checkMocorightFile == null) {
                                getFile = getFileMDRM(mocorightJWT.download_url)
                            } else {
                                println("FILE MOCORIGHT EXIST")
                                val fileByte: ByteArray = Base64.getDecoder().decode(checkMocorightFile)
                                getFile = FileMoco(
                                    status = "OK",
                                    fileMDRM = fileByte
                                )
                            }
                            if (getFile.status == "OK") {
                                var decodeMetaright = MocorightClass.decode(
                                    mocorightJWT.moco_right,
                                    inputList(
                                        appID = appid,
                                        userID = userid,
                                        pustakaID = selectedLibs
                                    )
                                )
                                if (decodeMetaright.status === "OK") {
                                    var containerkey = decodeMetaright.containerKey!!
                                    var aad = decodeMetaright.aad!!
                                    var pdfkey = Base64.getEncoder()
                                        .encodeToString(decodeMetaright.pdfKey)
                                    var file = getFile.fileMDRM!!
                                    val pdfFile =
                                        MocorightClass.decryptFileByteArray(containerkey, aad, file)
                                    println("hasil decrtpy pdfile : " + pdfFile)
                                    pdfView?.fromBytes(pdfFile)?.password(pdfkey)?.load()
                                    Handler(Looper.getMainLooper()).post {
                                        if(checkMocorightLocal == null){
                                            text1.text =
                                                "Status : " + decodeMetaright.status + "\nContainerkey : " + Base64.getEncoder()
                                                    .encodeToString(decodeMetaright.containerKey) + "\nPdfkey : " + Base64.getEncoder()
                                                    .encodeToString(decodeMetaright.pdfKey) + "\nBorrow ID: " + decodeMetaright.borrowID + "\nMDRM ID : " + decodeMetaright.mdrmID

                                        } else {
                                            text1.text =
                                                "Mocoright Record Exist\n" + "Status : " + decodeMetaright.status + "\nContainerkey : " + Base64.getEncoder()
                                                    .encodeToString(decodeMetaright.containerKey) + "\nPdfkey : " + Base64.getEncoder()
                                                    .encodeToString(decodeMetaright.pdfKey) + "\nBorrow ID: " + decodeMetaright.borrowID + "\nMDRM ID : " + decodeMetaright.mdrmID

                                        }
                                                                                text1.visibility = View.INVISIBLE
                                        pdfView.visibility = View.VISIBLE
                                        but2.isEnabled = true
                                        but2.text = "HIDE PDF"
                                        but1.text = "OPEN"
                                        but1.isEnabled = false
                                        spinner.visibility = View.INVISIBLE
                                        mocoSpin.visibility = View.INVISIBLE

                                    }
                                } else {
                                    Handler(Looper.getMainLooper()).post {
                                        text1.text =
                                            "Status Borrow : " + decodeMetaright.status
                                        but2.isEnabled = false
                                        but2.text = "SHOW PDF"
                                    }
                                }
                            } else {
                                Handler(Looper.getMainLooper()).post {
                                    text1.text = getFile.status
                                }
                            }
                        } else {
                            Handler(Looper.getMainLooper()).post {
                                text1.text = mocorightJWT.status
                            }
                        }
                    } else {
                        Handler(Looper.getMainLooper()).post {
                            text1.text = attStatus + ", please relogin"
                        }
                    }
                }
            }, 2000)

        }
        binding.buttonSec.setOnClickListener {
            if (pdfView.isInvisible) {
                but2.text = "HIDE PDF"
                but1.isEnabled = false
                text1.visibility = View.INVISIBLE
                spinner.visibility = View.INVISIBLE
                mocoSpin.visibility = View.INVISIBLE
                pdfView.visibility = View.VISIBLE
            } else {
                but2.text = "SHOW PDF"
                but1.isEnabled = true
                text1.visibility = View.VISIBLE
                spinner.visibility = View.VISIBLE
                mocoSpin.visibility = View.VISIBLE
                pdfView.visibility = View.INVISIBLE
            }
        }


    }

    fun getMocoright(): MocoRightData {
        var result: MocoRightData
        runBlocking {
            val a = async { fuelGetMocoright() }
            result = a.await()
        }
        return result
    }

    fun fuelGetMocoright(): MocoRightData {
        val serverurl = context?.let { getConfigValue(it, "serverurl") }!!
            var sharedPref : SharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE);
        val bodyData =
            """{"moco_id": "$selectedMoco","library_id": "$selectedLibs","user_id": "$userid","attest_certificate_id": "$acID" }"""
        println(bodyData)
        val (request, response, result) = Fuel.post("${serverurl}/api/distribution/distribute")
            .header(Headers.CONTENT_TYPE, "application/json")
            .body(bodyData).responseString()
        when (result) {
            is Result.Failure -> {
                val ex = result.getException()
                println(String(response.data))
                println(request)
                println(ex)
                try {
                    val mainObject = JSONObject(String(response.data))
                    val stat = mainObject.getString("details")
                    return MocoRightData(
                        status = stat,
                        download_url = "",
                        moco_right = ""
                    )
                } catch(e: Exception) {
                    return MocoRightData(
                        status = "Failed to get Mocoright, Please check ur connection",
                        download_url = "",
                        moco_right = ""
                    )
                }
            }
            is Result.Success -> {
                val data = result.get()
                println("result getMocoRight : $data")
                val mainObject = JSONObject(data)
                sharedPref.edit().putString("$acID:$selectedLibs:$selectedMoco:jwt", data).apply()
                val downloadUrl = mainObject.getString("download_url")
                val mocorightJWT = mainObject.getString("moco_right")
                return MocoRightData(
                    status = "OK",
                    download_url = downloadUrl,
                    moco_right = mocorightJWT
                )
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getFileMDRM(url: String): FileMoco {

        var result: FileMoco = FileMoco(
            status = "",
            fileMDRM = null
        )
        runBlocking {
            val a = async { downloadFile(url) }
            result = a.await()
        }
        return result
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun downloadFile(url: String): FileMoco {
        val urlAddress: String = url
        var sharedPref : SharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE);
        return try {
            val url = URL(urlAddress)
            val httpURLConnection: HttpURLConnection = url.openConnection() as HttpURLConnection
            httpURLConnection.requestMethod = "GET"
            httpURLConnection.doInput = true
            httpURLConnection.connect()
            val fileLength: Int = httpURLConnection.contentLength
            val inputStream: InputStream = httpURLConnection.inputStream
            val buffer = ByteArray(1024)
            var curLength = 0
            var newLength = 0
            val byteArrayOutputStream = ByteArrayOutputStream()
            while (inputStream.read(buffer).also { newLength = it } > 0) {
                curLength += newLength
                byteArrayOutputStream.write(buffer, 0, newLength);
            }
            if (fileLength > 0 && curLength != fileLength) {
                val result = arrayOfNulls<Byte>(1)
                println("ERROR_FILE_LENGTH")
                return FileMoco(
                    status = "ERROR_FILE_LENGTH",
                    fileMDRM = null
                )
            }
            val resultByteArray: ByteArray = byteArrayOutputStream.toByteArray()
            println("size file download = " + resultByteArray.size)
            sharedPref.edit().putString("$acID:$selectedLibs:$selectedMoco:file", Base64.getEncoder().encodeToString(resultByteArray)).apply()
            return FileMoco(
                status = "OK",
                fileMDRM = resultByteArray
            )
        } catch (e: IOException) {
            e.printStackTrace()
            println("ERROR_DOWNLOAD_FAILED")
            return FileMoco(
                status = "ERROR_DOWNLOAD_FAILED",
                fileMDRM = null
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun getConfigValue(context: Context, name: String?): String? {
        val resources = context.resources
        try {
            val rawResource = resources.openRawResource(R.raw.config)
            val properties = Properties()
            properties.load(rawResource)
            return properties.getProperty(name).toString()
        } catch (e: Resources.NotFoundException) {
            Log.e("file", "Unable to find the config file: " + e.message)
        } catch (e: IOException) {
            Log.e("file", "Failed to open config file.")
        }
        return null
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                        return true
                    }
                }
            }
        }
        return false
    }

    // Storage Permissions
    private val REQUEST_EXTERNAL_STORAGE = 1
    private val PERMISSIONS_STORAGE = arrayOf<String>(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.MANAGE_EXTERNAL_STORAGE
    )

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    fun verifyStoragePermissions(activity: Activity?) {
        // Check if we have write permission
        val permission = ActivityCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            if (activity != null) {
                ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
                )
            }
        }
    }
}

